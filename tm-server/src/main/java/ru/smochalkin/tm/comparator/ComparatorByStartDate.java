package ru.smochalkin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.model.IHasStartDate;

import java.util.Comparator;

public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    @NotNull
    public final static ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    @NotNull
    private ComparatorByStartDate() {
    }

    @Override
    public int compare(@NotNull IHasStartDate o1, @NotNull IHasStartDate o2) {
        if (o1.getStartDate() == null) return 1;
        if (o2.getStartDate() == null) return -1;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
