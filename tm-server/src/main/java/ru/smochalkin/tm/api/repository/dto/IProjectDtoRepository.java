package ru.smochalkin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IRepositoryDto;
import ru.smochalkin.tm.dto.ProjectDto;

import java.util.List;

public interface IProjectDtoRepository extends IRepositoryDto<ProjectDto> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDto> findAll();

    @NotNull
    List<ProjectDto> findAllByUserId(@Nullable String userId);

    @NotNull
    ProjectDto findById(@Nullable String id);

    @Nullable
    ProjectDto findByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @Nullable
    ProjectDto findByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    @Nullable
    ProjectDto findByName(
            @Nullable String userId, @NotNull String name
    );

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeByName(
            @Nullable String userId, @NotNull String name
    );

    void removeByIndex(@NotNull String userId, int index);

    int getCount();

    int getCountByUser(@NotNull String userId);

}
