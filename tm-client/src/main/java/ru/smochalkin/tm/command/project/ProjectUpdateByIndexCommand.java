package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        index--;
        System.out.print("Enter new name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("Enter new description: ");
        @Nullable final String desc = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getProjectEndpoint().updateProjectByIndex(serviceLocator.getSession(), index, name, desc);
        printResult(result);
    }

}
